//
// Created by Frix on 30.06.2021.
//

#ifndef SEMESTRALKA_NAVIGATIONCOMPONENT_H
#define SEMESTRALKA_NAVIGATIONCOMPONENT_H


#include <cstdlib>
#include <iostream>
#include "Pojo/Position.h"

class NavigationComponent {

    // Pismena v konzoli jsou priblizne 2.5x vyssi nez sirsi.
    constexpr static const float consoleToSquareRatio = 2.5;

public:

    static const int LEFT = 0;
    static const int RIGHT = 1;
    static const int TOP = 2;
    static const int BOTTOM = 3;
    static const int SAME = 4;

    // Zjisti na ktere strane se od base Widgetu nachazi druhy Widget.
    // Metoda nepocita s sirkou objektu. Doporucuje se pouzit pozici stredu objektu.
    static int positionSquare(Position positionBase,
                              Position positionCandidate) {
        if(positionBase==positionCandidate){return SAME;}
        // Kvuli vnitrni reprezentaci bitmap jsou souradnice brane od horniho leveho rohu.
        // pro pocitani, na ktere strane se nachazi od sebe dva elementy prohazuji bazi a kandidata,
        // protoze se mi lepe naklada se souradnicemi z leveho spodniho rohu.
        int baseX = positionCandidate.getX();
        int baseY = positionCandidate.getY();
        int candidateX = positionBase.getX();
        int candidateY = positionBase.getY();

        float candidateRelativeXdistance = abs(candidateX-baseX);
        float candidateRelativeYdistance = abs(candidateY-baseY);

        // Bound je cara, ktera oddeluje Horni/Dolni a Leve/Prave umisteni.
        float boundY = candidateRelativeXdistance/consoleToSquareRatio;
        if(candidateRelativeYdistance>boundY){
            if((candidateY-baseY)>0) {
                return TOP;
            }else{
                return BOTTOM;
            }
        }else{
            if((candidateX-baseX)>0) {
                return LEFT;
            }else{
                return RIGHT;
            }
        }

        return 0;
    }

};


#endif //SEMESTRALKA_NAVIGATIONCOMPONENT_H
