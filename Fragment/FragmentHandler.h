//
// Created by Frix on 02.08.2021.
//

#ifndef SEMESTRALKA_FRAGMENTHANDLER_H
#define SEMESTRALKA_FRAGMENTHANDLER_H

#include <string>
#include <functional>
#include <memory>
#include <map>
#include <iostream>
#include <chrono>
#include <thread>
#include "../Navigation/Fragment.h"
#include "../KeyMapComparator.h"

using namespace std;

/*
 * Slouzi k obsluze fragmentu.
 * Funkcemi lze prepinat a posilat stavy mezi fragmenty.
 * Zpracovava komunikaci klavesoveho vstupu a fragmentu.
 */
class FragmentHandler: public FragmentListener{
    map<string, shared_ptr<Fragment>> dummyMap;
    // Mapa vsech fragmentu aplikace, indexovanych podle jejich Const jmen.
    map<string, shared_ptr<Fragment>> & mFragments=dummyMap;

    map<char, function<void()>> dummyKeyMap;
    // Observer funkce, indexovane dle znaku, kterymi se maji aktivovat
    map<char, function<void()>>& mKeyMap=dummyKeyMap;
public:
    /// @param defaultFragment Const nazev fragmentu, ktery je vychozi
    /// @param fragments reference na vsechny fragmenty aplikace
    FragmentHandler(const std::string& defaultFragment,map<std::string, shared_ptr<Fragment>>&fragments){
        mFragments=fragments;

        // Prechod na defaultni fragment.
        ExchangeBundle defaultExchangeBundle;
        string mDef = defaultFragment;
        // TODO: Věci CLionu
        onFragmentChange(mDef,defaultExchangeBundle);

        // Zapnuti naslouchavani klavesnice
        startInputLoop();
    }

    /// Slouzi k naslouchavani stisknutych znaku. Znak 'k' je rezervovan na konec loopu (potazmo programu). Ostatni klavesy si urcuji fragmenty.
    void startInputLoop(){
        bool run = true;
        while(run)
        {
            char ch = std::cin.get();
            // Rezervovano
            if(ch=='k') {
                run=false;
            }else{
                // Pokud je namapovany provest, jinak bezet dal.
                if(!mKeyMap.empty()){
                    auto foundRef = mKeyMap.find(ch);
                    if(foundRef!=mKeyMap.end()){
                        (foundRef->second());
                    }
                }
            }
            if(run) {
                // STACKOVERFLOW
                using namespace this_thread; // sleep_for, sleep_until
                using namespace chrono; // nanoseconds, system_clock, seconds
                sleep_for(nanoseconds(10));
                // KONEC STACKOVERFLOW
            }
        }
    }

    /// Zmeni mapovani klaves na Observery.
    /// @param keyMap mapa klavesovych znaku a patricnych funkci.
    void onKeyMapChange(map<char, function<void()>>& keyMap) override{
        FragmentHandler::mKeyMap = keyMap;
    }

    /// Zmeni aktivni Fragment na jiny.
    /// Prave aktivni fragment se prestane vypisovat na obrazovku a pri dalsi aktivaci se zavola jeho onCreate metoda.
    /// @param fragmentName Const nazev fragmentu
    /// @param exchangeBundle Bundle pro prenos informaci pri transakci
    void onFragmentChange(std::string& fragmentName, ExchangeBundle&exchangeBundle) override{
        auto foundRef = mFragments.find(fragmentName);
        if(foundRef==mFragments.end())
        {
            throw "Zadany fragment neexistuje.";
        } else{
            foundRef->second->onCreate(this,exchangeBundle);
        }
    }
};

#endif //SEMESTRALKA_FRAGMENTHANDLER_H
