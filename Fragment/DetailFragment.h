//
// Created by Frix on 04.08.2021.
//

#ifndef SEMESTRALKA_DETAILFRAGMENT_H
#define SEMESTRALKA_DETAILFRAGMENT_H

#include <functional>
#include <map>
#include "../Navigation/FragmentListener.h"
#include "../Creators/DetailCreator.h"
#include "../Navigation/Fragment.h"
#include "../Gui/Widgets/Layout.h"
#include "../Gui/Widgets/PlannerLayout.h"

using namespace std;
class DetailFragment : public Fragment{
    map<char, function<void()>> mKeyMap;
    FragmentListener *mFragmentListener;
    Layout mDetailLayout = Layout(true, Position(0, 0));
    shared_ptr<PlannerLayout> mPlannerLayout = make_shared<PlannerLayout>(Position(20,4));

    void onCreate(FragmentListener * fragmentListener, ExchangeBundle & exchangeBundle) override{
        Const constants;

        DetailCreator detailCreator;
        mFragmentListener=fragmentListener;
        mDetailLayout.appendCreator(detailCreator);
        mDetailLayout.setDimensions(81, 14);
        mPlannerLayout->addNewEvent(Event(2000,3000,"aa","Behani s obruci",""));
        mPlannerLayout->addNewEvent(Event(1000000,1005000,"aad","Svatecni obed",""));
        mDetailLayout.addInnerLayout(mPlannerLayout);

        mPlannerLayout->update();
        mDetailLayout.update();

        shared_ptr<Text> monthNameText = detailCreator.findTextByName(constants.FRAGMENT_DETAIL_EXCHANGE_CHOOSEN_DAY);

        string exchangeS=constants.FRAGMENT_DETAIL_EXCHANGE_CHOOSEN_DAY;
        string& day = exchangeBundle.getBundleElement(exchangeS);

        time_t choosenDay = stoi(day);
        std::tm t = {};
        t = *gmtime(&choosenDay);
        std::cout << std::asctime(&t) << endl;
       // string together = mDay+". "+mMonth+". "+mYear;
       string together = std::asctime(&t);
       string togetherCut = together.substr(0, together.size()-1);
       monthNameText->setText(togetherCut);

        mKeyMap['f']=([this]() {
            string switchToFragment = "CalendarFragment";
            ExchangeBundle exchangeBundle;
            mFragmentListener->onFragmentChange(switchToFragment,exchangeBundle);
        });

        fragmentListener->onKeyMapChange(mKeyMap);
    }
};


#endif //SEMESTRALKA_DETAILFRAGMENT_H
