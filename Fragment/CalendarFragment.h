//
// Created by Frix on 02.08.2021.
//

#ifndef SEMESTRALKA_CALENDARFRAGMENT_H
#define SEMESTRALKA_CALENDARFRAGMENT_H

#include <functional>
#include <map>
#include "../Navigation/Fragment.h"
#include "../Creators/MainCreator.h"
#include "../Navigation/FragmentListener.h"
#include "../Const.h"
#include "../Gui/Widgets/Layout.h"
#include "../Gui/Widgets/CalendarLayout.h"

using namespace std;
class CalendarFragment: public Fragment{
    map<char, function<void()>> mKeyMap;
    Layout mMainLayout = Layout(true, Position(0, 0));
    shared_ptr<CalendarLayout> mCalendar = make_shared<CalendarLayout>(Position(25, 4),
                                                           [this](time_t choosenDate){
        onCalendarDateClick(choosenDate);
    });

    FragmentListener* mFragmentListener;

    void onCalendarDateClick(time_t choosenDate){
        Const constants;
        string switchToFragment = constants.FRAGMENT_NAME_DETAIL;

        std::tm t = {};
        t = *gmtime(&choosenDate);
        std::cout << std::asctime(&t) << endl;
        // string together = mDay+". "+mMonth+". "+mYear;
        string together = std::asctime(&t);


        ExchangeBundle exchangeBundle;
        exchangeBundle.setBundleElement(constants.FRAGMENT_DETAIL_EXCHANGE_CHOOSEN_DAY, to_string(choosenDate));
        mFragmentListener->onFragmentChange(switchToFragment,exchangeBundle);
    }

    void onCreate(FragmentListener * fragmentListener, ExchangeBundle & bundle) override{
        mFragmentListener=fragmentListener;
        mCalendar->setWidgetName("mCalendar");
        MainCreator mainCreator;
        mainCreator.findButtonByName("nextButton")->setOnClickListener([this]() {
            (mCalendar)->nextMonth();
        });
        mainCreator.findButtonByName("previousButton")->setOnClickListener([this]() {
            (mCalendar)->previousMonth();
        });

        mMainLayout.setWidgetName("Main layout");
        mMainLayout.setDimensions(81, 14);
        mMainLayout.appendCreator(mainCreator);
        mMainLayout.addInnerLayout(mCalendar);

        mCalendar->update();
        mMainLayout.update();
        mMainLayout.initFocus();

        mKeyMap['a']=([this]() {
            mMainLayout.changeFocus(NavigationComponent::LEFT);
        });
        mKeyMap['d']=([this]() {
            mMainLayout.changeFocus(NavigationComponent::RIGHT);
        });
        mKeyMap['w']=([this]() {
            mMainLayout.changeFocus(NavigationComponent::TOP);
        });
        mKeyMap['s']=([this]() {
            mMainLayout.changeFocus(NavigationComponent::BOTTOM);
        });
        mKeyMap['e']=([this]() {
            mMainLayout.clickFocusedButton();
        });
        mKeyMap['n']=([this](){
            mCalendar->nextMonth();
        });
        mKeyMap['p']=([this](){
            mCalendar->previousMonth();
        });
        mKeyMap['f']=([this]() {
            Const constaints;
            string switchToFragment = "DetailFragment";
            ExchangeBundle exchangeBundle;
            exchangeBundle.setBundleElement(constaints.FRAGMENT_DETAIL_EXCHANGE_CHOOSEN_DAY, "1");
            mFragmentListener->onFragmentChange(switchToFragment,exchangeBundle);
        });

        fragmentListener->onKeyMapChange(mKeyMap);
    }
};


#endif //SEMESTRALKA_CALENDARFRAGMENT_H
