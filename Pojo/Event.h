//
// Created by Frix on 12.08.2021.
//

#ifndef SEMESTRALKA_EVENT_H
#define SEMESTRALKA_EVENT_H

#include <string>
#include <utility>

using namespace std;


class Event {
private:
    time_t mBegin=0;
    time_t mEnd=0;
    string mUid="0";
    string mDescription="Undefined";
    string mLocation="Pardubice";

public:
    Event(time_t start, time_t end, string uid, string description, string location)
    : mBegin(start), mEnd(end), mUid(std::move(uid)), mDescription(std::move(description)), mLocation(std::move(location))
    {

    }
    Event()= default;

    time_t getBegin() const {
        return mBegin;
    }

    time_t getEnd() const {
        return mEnd;
    }

    const string &getUid() const {
        return mUid;
    }

    const string &getDescription() const {
        return mDescription;
    }

    const string &getLocation() const {
        return mLocation;
    }

};

#endif //SEMESTRALKA_EVENT_H
