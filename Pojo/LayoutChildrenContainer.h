//
// Created by Frix on 29.06.2021.
//

#ifndef SEMESTRALKA_LAYOUTCHILDRENCONTAINER_H
#define SEMESTRALKA_LAYOUTCHILDRENCONTAINER_H


#include <vector>
#include "Position.h"
#include "../Gui/Widgets/Widget.h"

class LayoutChildrenContainer{
    char mPriority;
    int mPosX;
    int mPosY;
    Widget& mWidget;
public:

    int getPosX() const{return mPosX;}

    int getPosY() const{return mPosY;}

    char getPriority()const{return mPriority;}

    Widget& getWidget() const {
        return mWidget;
    }

    LayoutChildrenContainer(Widget &mWidget,Position position, char priority) : mWidget(mWidget) {
        mPosX=position.getX();
        mPosY=position.getY();
        mPriority=priority;
    }

    bool operator==(const LayoutChildrenContainer& rhs) const &{
        return (rhs.getWidget().getUniqueId()==this->getWidget().getUniqueId());
    }
};


#endif //SEMESTRALKA_LAYOUTCHILDRENCONTAINER_H
