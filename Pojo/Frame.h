//
// Created by Frix on 30.06.2021.
//

#ifndef SEMESTRALKA_FRAME_H
#define SEMESTRALKA_FRAME_H


class Frame {
    char mLeft=0;
    char mRight=0;
    char mTop=0;
    char mBottom=0;

public:
    Frame& setLeft(char c){
        mLeft=c;
        return *this;
    }
    Frame& setRight(char c){
        mRight=c;
        return *this;
    }
    Frame& setTop(char c){
        mTop=c;
        return *this;
    }
    Frame& setBottom(char c){
        mBottom=c;
        return *this;
    }

    bool isActive() const{
        return (mLeft||mRight||mBottom||mTop);
    }

    int frameWidth() const{
        int add=0;
        if(mLeft!=0)add++;
        if(mRight!=0)add++;
        return add;
    }

    int frameHeight() const{
        int add=0;
        if(mTop!=0)add++;
        if(mBottom!=0)add++;
        return add;
    }

    char getLeft() const {
        return mLeft;
    }

    char getRight() const {
        return mRight;
    }

    char getTop() const {
        return mTop;
    }

    char getBottom() const {
        return mBottom;
    }
};


#endif //SEMESTRALKA_FRAME_H
