//
// Created by Frix on 01.07.2021.
//

#include "Position.h"

int Position::getX() const {
    return x;
}

int Position::getY() const {
    return y;
}

Position::Position(int x, int y) : x(x), y(y) {}

void Position::setX(int x) {
    Position::x = x;
}

void Position::setY(int y) {
    Position::y = y;
}

