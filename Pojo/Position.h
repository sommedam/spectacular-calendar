//
// Created by Frix on 01.07.2021.
//

#ifndef SEMESTRALKA_POSITION_H
#define SEMESTRALKA_POSITION_H


class Position {
    int x;
    int y;



public:
    Position(int x, int y);

    int getX() const;

    int getY() const;

    void setX(int x);

    void setY(int y);

    bool operator == (Position &second) const{
        if(x == second.getX() &&
        y == second.getY()){
            return true;
        }else{
            return false;
        }
    }

    Position operator +(Position&b) const   {
        return Position(x+b.getX(),y+b.getY());
    }
};


#endif //SEMESTRALKA_POSITION_H
