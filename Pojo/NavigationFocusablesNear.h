//
// Created by Frix on 05.07.2021.
//

#ifndef SEMESTRALKA_NAVIGATIONFOCUSABLESNEAR_H
#define SEMESTRALKA_NAVIGATIONFOCUSABLESNEAR_H


#include "../Gui/Widgets/Button.h"
#include "../Gui/Widgets/CalendarButton.h"

class NavigationFocusablesNear {
    Button& mBasicButton;
    CalendarButton& mCalendarButton;
    char mCloser;

public:

    NavigationFocusablesNear(Button &basicButton, CalendarButton &calendarButton, char closer) : mBasicButton(
            basicButton), mCalendarButton(calendarButton), mCloser(closer) {
    }


    Button &getBasicButton() const {
        return mBasicButton;
    }

    CalendarButton &getCalendarButton() const {
        return mCalendarButton;
    }

    char getCloser() const {
        return mCloser;
    }

    static const int CALENDAR=0;
    static const int BASIC=1;
};


#endif //SEMESTRALKA_NAVIGATIONFOCUSABLESNEAR_H
