//
// Created by Frix on 30.06.2021.
//

#ifndef SEMESTRALKA_CALENDARBUTTON_H
#define SEMESTRALKA_CALENDARBUTTON_H

#include "Button.h"

class CalendarButton : public Button {
public:
    const static int WIDTH = 4;
    const static int HEIGHT = 1;
    std::vector<char> widthVector{};

    explicit CalendarButton(int day=0,Position position=Position(0,0)):Button(Frame().setLeft('>'),position) {
        Button::mWidth = WIDTH;
        Button::mHeight = HEIGHT;

        widthVector.reserve(mWidth);
        widthVector.push_back(' ');
        if (day < 10) {
            widthVector.push_back('0' + day);
            widthVector.push_back(' ');
        } else {
            int lwr = (day % 10);
            widthVector.push_back('0' + (day / 10));
            widthVector.push_back('0' + lwr);
        }
        widthVector.push_back(' ');
        Button::bitmap.push_back(widthVector);
    }
};


#endif //SEMESTRALKA_CALENDARBUTTON_H
