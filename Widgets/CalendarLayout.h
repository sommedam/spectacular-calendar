//
// Created by Frix on 28.06.2021.
//

#ifndef SEMESTRALKA_CALENDARLAYOUT_H
#define SEMESTRALKA_CALENDARLAYOUT_H

#include <iostream>
#include <sstream>
#include <utility>
#include <vector>
#include "Widget.h"
#include "Layout.h"
#include "Button.h"
#include "TextButton.h"
#include "CalendarButton.h"
#include "../Creators/CalendarCreator.h"


class CalendarLayout: public Layout {
    int rows = 6;
    int columns = 7;

    const static int TOP_OFFSET = 2;

    // Prvni den v prave zobrazenem mesici
    time_t mFirstDayOfMonth = time(nullptr);
    CalendarCreator calendarCreator;
    std::function<void(time_t)> mCalendarObserver;
public :
    explicit CalendarLayout(Position position,std::function<void(time_t)> calendarObserver) : Layout(false, position) {
        mCalendarObserver=std::move(calendarObserver);
    // Nastaví mFirstDayOfMonth na první den aktuálního měsíce.
        defaultTime();
        // Rozmery kalendare jsou presne tak velke, aby obsahly
        // cisla jednotlivych dni + vertikalni padding.
        mWidth = columns * CalendarButton::WIDTH;
        mHeight = rows * CalendarButton::HEIGHT + TOP_OFFSET;
        Layout::setDimensions(mWidth, mHeight);
        listMonth();
        Layout::appendCreator(calendarCreator);

        //calendarCreator.setOnBitmapChangeListener(this);
        calendarCreator.setParentPosition(position);
    }

    static int daysToSeconds(int d) {
        return d * 24 * 60 * 60;
    }

    /* nextMonth připraví mFirstDayOfMonth na nový měsíc, zavolá předělání widgetů
     * a celý layout se updatuje
     */
    void nextMonth() {
        int notPreciseMonth = daysToSeconds(31);
        mFirstDayOfMonth += notPreciseMonth;
        std::tm t = {};
        t = *gmtime(&mFirstDayOfMonth);
        mFirstDayOfMonth -= daysToSeconds(t.tm_mday - 1);
        listMonth();
        std::cout << std::asctime(&t) << endl;
    }

    void previousMonth() {
        int notPreciseMonth = daysToSeconds(1);
        mFirstDayOfMonth -= notPreciseMonth;
        std::tm t = {};
        t = *gmtime(&mFirstDayOfMonth);
        mFirstDayOfMonth -= daysToSeconds(t.tm_mday - 1);
        listMonth();
    }

    void defaultTime() {
        mFirstDayOfMonth = time(nullptr);
        mFirstDayOfMonth += 60 * 60 * 2;
        std::tm t = {};
        t = *gmtime(&mFirstDayOfMonth);
        std::cout << std::asctime(&t) << endl;
        // GMT+2
        // Odecist pocet dni, aby se prvni den mesice nastavil dobre
        mFirstDayOfMonth -= ((t.tm_mday - 1) * 24 * 60 * 60);
    }

    int numberOfDays = 0;

    void listMonth() {
        std::tm t = {};
        int previousMonthNumberOfDays = numberOfDays;
        // Smazání všech již nepotřebných čísel "datumů".
        for (int k = 0; k < previousMonthNumberOfDays; k++) {
            string nameToErase = to_string(k + 1);
            calendarCreator.removeButtonByName(nameToErase);
        }

        t = *gmtime(&mFirstDayOfMonth);
        string monthName;
        int year = t.tm_year + 1900;
        switch (t.tm_mon) {
            case 0:
                monthName = "Leden";
                numberOfDays = 31;
                break;
            case 1:
                monthName = "Unor";
                if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) {
                    numberOfDays = 29;
                } else {
                    numberOfDays = 28;
                }
                break;
            case 2:
                monthName = "Brezen";
                numberOfDays = 31;
                break;
            case 3:
                monthName = "Duben";
                numberOfDays = 30;
                break;
            case 4:
                monthName = "Kveten";
                numberOfDays = 31;
                break;
            case 5:
                monthName = "Cerven";
                numberOfDays = 30;
                break;
            case 6:
                monthName = "Cervenec";
                numberOfDays = 31;
                break;
            case 7:
                monthName = "Srpen";
                numberOfDays = 31;
                break;
            case 8:
                monthName = "Zari";
                numberOfDays = 30;
                break;
            case 9:
                monthName = "Rijen";
                numberOfDays = 31;
                break;
            case 10:
                monthName = "Listopad";
                numberOfDays = 30;
                break;
            case 11:
                monthName = "Prosinec";
                numberOfDays = 31;
                break;
            default:
                monthName = "Invalid Month number";
                break;
        }
        cout<<"------ number of days:"<<numberOfDays<<endl;
        // Nastavení prvního dne měsíce.
        // Inicializováno jako záporné číslo.
        // Po iteraci s inkrementací od pondělí je pak den 1 prvním dnem k zobrazení.
       int day = -t.tm_wday + 2;
        if (day == 2) { day = -5; }
        for (int i = 0; i < rows; i++) {
            // 7 cisel na radek. 5 znaku na cislo
            for (int j = 0; j < columns; j++) {
                // 5 znaku na cislo
                int buttonPositionX = j * CalendarButton::WIDTH;
                int buttonPositionY = i * CalendarButton::HEIGHT + TOP_OFFSET;
                if (day > 0 && day <= numberOfDays) {
                    cout<<"init day: "<<day<<endl;
                    shared_ptr<CalendarButton> buttonToAdd = make_shared<CalendarButton>(day, Position(buttonPositionX,buttonPositionY));
                    buttonToAdd->setWidgetName(to_string(day));
                    buttonToAdd->setOnClickListener([this, day](){
                        time_t iButtonTime = mFirstDayOfMonth + daysToSeconds(day-1);
                        mCalendarObserver(iButtonTime);
                    });
                    calendarCreator.addButton(buttonToAdd);
                }
                day++;
            }
        }

        monthName += " " + to_string(year);
        calendarCreator.findTextByName("monthNameText")->setText(monthName);
        Layout::update();
    }


    int getWidth() const override {
        return mWidth;
    }

    int getHeight() const override {
        return mHeight;
    }
};


#endif //SEMESTRALKA_CALENDARLAYOUT_H
