//
// Created by Frix on 29.06.2021.
//

#ifndef SEMESTRALKA_BUTTON_H
#define SEMESTRALKA_BUTTON_H

#include <functional>
#include <utility>
#include "Widget.h"
#include "../Pojo/Frame.h"

using namespace std;

/// NxM rozmerna bitmapa, ktera zmeni vzhled pri zamereni a upozorni Observer při layout kliknuti.
class Button: public Widget {
protected:
    // Pokud je tlacitko zamereno
    bool active = false;
    int mWidth = 0;
    int mHeight = 0;
    std::function<void()> mOnClickObserver;
    // Deaktivovani click observeru probiha zmenou tohoto boolu.
    bool mOnClickObserverActive = false;

    vector<vector<char>> bitmap;
    // Ramecek, ktery je okolo tlacitka.
    Frame frame;
public:
    explicit Button(Position position = Position(0, 0)) : Widget(position) {
        frame = Frame();
    }

    Button(const Frame &frame, Position position) : frame(frame), Widget(position) {}

    // Button ma pro potreby ramecku posunute kraje o 1 na vsech stranach.
    char getBitmapXY(int x, int y) const override {
        if (active) {
            if (frame.getLeft()) {
                x--;
            }
            if (frame.getTop()) {
                y--;
            }
            // Pokud se pta na krajni, vratit pozadi.
            // Pokud je nulovy ramecek, tak nevynechat.
            if (frame.isActive()) {
                if (x == -1) {
                    return frame.getLeft();
                }

                if (y == -1) {
                    return frame.getTop();
                }

                if (frame.getBottom()) {
                    if (y == getHeight() - frame.frameHeight()) {
                        return frame.getBottom();
                    }
                }

                if (frame.getRight()) {

                    if (x == getWidth() - frame.frameWidth()) {
                        return frame.getRight();
                    }
                }
            }
        }
        if (x >= getWidth() || x < 0) {
            throw "Mimo oblast widgetu.";
        }
        if (y >= getHeight() || y < 0) {
            throw "Mimo oblast widgetu.";
        }
        char ret = bitmap[y][x];
        return (ret);
    }

    // Zvetsena sirka o ramecek napravo a nalevo.
    int getWidth() const override {
        if (active) {
            return mWidth + frame.frameWidth();
        }
        return mWidth;
    }

    // Zvetsena vyska o ramecek dole a nahore.
    int getHeight() const override {
        if (active) {
            return mHeight + frame.frameHeight();
        }
        return mHeight;
    }

    void click(){
        if(mOnClickObserverActive){
            mOnClickObserver();
        }
    }

    void setOnClickListener(std::function<void()> observer){
        mOnClickObserver=move(observer);
        mOnClickObserverActive=true;
    }
    void removeListener(){
        mOnClickObserverActive=false;
    }

    /// Zapne zamereni na tlacitko. Prizpusobi se tomu okraje aj.
    void onFocusStart() {
        active = true;
        Widget::forceBitmapUpdate();
    }

    // Vypne zamereni na tlacitko.
    void onFocusEnd() {
        active = false;
        Widget::forceBitmapUpdate();
    }
};


#endif //SEMESTRALKA_BUTTON_H
