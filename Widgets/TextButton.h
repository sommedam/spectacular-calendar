//
// Created by Frix on 29.06.2021.
//

#ifndef SEMESTRALKA_TEXTBUTTON_H
#define SEMESTRALKA_TEXTBUTTON_H


#include <vector>
#include "Button.h"


class TextButton : public Button {

public:
    explicit TextButton(const string &text, Position position) : Button(Frame().setLeft('>').setRight('<'), position) {
        Button::mWidth = text.length() + 2;
        Button::mHeight = 1;

        std::vector<char> widthVector;
        widthVector.reserve(mWidth);
        widthVector.push_back('[');
        for (char i : text) {
            widthVector.push_back(i);
        }
        widthVector.push_back(']');
        Button::bitmap.push_back(widthVector);
    }
};


#endif //SEMESTRALKA_TEXTBUTTON_H
