//
// Created by Frix on 11.08.2021.
//

#ifndef SEMESTRALKA_PLANNERLAYOUT_H
#define SEMESTRALKA_PLANNERLAYOUT_H

#include "Layout.h"
#include "../Pojo/Event.h"
#include "CalendarLayout.h"
#include "../Creators/PlannerEventCreator.h"

using namespace std;
class PlannerLayout: public Layout{

    int mWidth = 40;
    int mHeight = 10;
    LayoutCreator mPlannerCreator;
    int eventCount=0;

public :
    explicit PlannerLayout(Position position) : Layout(false, position) {
    Layout::setDimensions(mWidth, mHeight);
    Layout::appendCreator(mPlannerCreator);
    mPlannerCreator.setParentPosition(position);
}


void addNewEvent(Event event){
    int positionY=(eventCount*2);
  //  if(eventCount!=0){positionY++;}
    shared_ptr<Layout> eventLayout =
            Layout::addInnerLayout(make_shared<Layout>(false,Position(0,positionY)));
    eventLayout->setWidgetName("New event layout");
    // TODO TODO TODO eventLayout->appendCreator(PlannerEventCreator(move(event),eventCount++));
    eventLayout->setDimensions(35,2);
    eventLayout->update();
}

int getWidth() const override {
    return mWidth;
}

int getHeight() const override {
    return mHeight;
}
};



#endif //SEMESTRALKA_PLANNERLAYOUT_H
