//
// Created by Frix on 29.06.2021.
//

#ifndef SEMESTRALKA_TEXT_H
#define SEMESTRALKA_TEXT_H


#include <string>
#include <utility>
#include "Widget.h"

class Text: public Widget {
    int mWidth=0;
    int mHeight=1;
    std::vector<std::vector<char>> bitmap;
    Position mPosition=Position(0,0);
public:
    explicit Text(std::string text,Position position) : Widget(position) {
        mWidth=text.length();
        mPosition=position;
        std::vector<char> widthVector;
        widthVector.reserve(mWidth);
        for (int i = 0; i < mWidth; ++i) {
            widthVector.push_back(text[i]);
        }
        bitmap.push_back(widthVector);

       /* Widget::setBitmap(bitmap);
        Widget::setWidth(mWidth);
        Widget::setHeight(mHeight);*/
    }

    void setText(std::string& text){
        mWidth=text.size();
        std::vector<char> widthVector;
        widthVector.reserve(mWidth);
        for (int i = 0; i < mWidth; ++i) {
            widthVector.push_back(text[i]);
        }
        bitmap.clear();
        bitmap.push_back(widthVector);
        Widget::forceBitmapUpdate();
    }

public:
    char getBitmapXY(int x, int y) const override {
        if(x >= mWidth || x < 0){
            throw "Mimo oblast widgetu.";
        }
        if(y >= mHeight || y < 0){
            throw "Mimo oblast widgetu.";
        }
        char r = bitmap[y][x];
        return r;
    }

    int getWidth() const override {
        return mWidth;
    }

    int getHeight() const override {
        return mHeight;
    }
};


#endif //SEMESTRALKA_TEXT_H
