//
// Created by Frix on 28.06.2021.
//

#ifndef SEMESTRALKA_WIDGET_H
#define SEMESTRALKA_WIDGET_H


#include <utility>
#include <vector>
#include <ctime>
#include <chrono>
#include <ratio>
#include <iostream>
#include <functional>
#include "../Pojo/Position.h"
#include "../Updatable.h"

class Widget {
    Position mParentPosition=Position(0,0);
    Position mPositionInLayout;
    int mWidth=0;
    int mHeight=0;
    std::vector<std::vector<char>> bitmap;

    Updatable* mOnBitmapChangeListener;
    bool mOnChangeObserverActive = false;

    // Slouzi pro jednoznacnou identifikaci
    long mUniqueId;
    std::string mWidgetName;
public:
    void setOnBitmapChangeListener(Updatable * observer) {
        std::string wName=Widget::getWidgetName();
        mOnBitmapChangeListener = observer;
        mOnChangeObserverActive = true;
    }
    void forceBitmapUpdate(){
        if(mOnChangeObserverActive){
            std::cout<<mOnChangeObserverActive<<std::endl;
            mOnBitmapChangeListener->update();
        }
    }

    explicit Widget(Position positionInLayout)
            : mPositionInLayout(positionInLayout){
        mUniqueId = std::chrono::duration_cast<std::chrono::microseconds>(
                std::chrono::steady_clock::now().time_since_epoch()).count()
                    + mWidth
                    + mHeight;
    }

public:
    void setWidgetName(const std::string &widgetName) {
        Widget::mWidgetName = widgetName;
    }

    const std::string getWidgetName()const{
        return mWidgetName;
    }

    virtual int getHeight ( )const=0;
    virtual int getWidth  ( )const=0;


    void setParentPosition(const Position &parentPosition) {
        Widget::mParentPosition = parentPosition;
    }

    void setWidth(int width) {
        Widget::mWidth = width;
    }

    void setHeight(int height) {
        Widget::mHeight = height;
    }


    Position getParentPosition() const {
        return mParentPosition;
    }
    Position getPositionInLayout() const {
        return mPositionInLayout;
    }


    virtual char getBitmapXY(int x, int y) const = 0;


    long getUniqueId() const {
        return mUniqueId;
    }

    void setBitmap(const std::vector<std::vector<char>> &bitmp) {
        Widget::bitmap = bitmp;
    }

};


#endif //SEMESTRALKA_WIDGET_H
