//
// Created by Frix on 28.06.2021.
//

#ifndef SEMESTRALKA_CALENDARLAYOUT_H
#define SEMESTRALKA_CALENDARLAYOUT_H

#include <iostream>
#include <sstream>
#include <utility>
#include <vector>
#include "Widget.h"
#include "Layout.h"
#include "Button.h"
#include "TextButton.h"
#include "CalendarButton.h"
#include "../../Creators/CalendarCreator.h"
#include "../../CalendarEssentials/DateIterator.h"
#include "../../CalendarEssentials/Months.h"


class CalendarLayout: public Layout {
    int rows = 6;
    int columns = 7;

    const static int TOP_OFFSET = 2;

    DateIterator dateIterator = DateIterator();
    CalendarCreator calendarCreator;
    std::function<void(time_t)> mCalendarObserver;
public :
    explicit CalendarLayout(Position position, std::function<void(time_t)> calendarObserver) : Layout(false, position) {
        mCalendarObserver = std::move(calendarObserver);
        // Nastaví mFirstDayOfMonth na první den aktuálního měsíce.
        dateIterator.defaultTime();
        // Rozmery kalendare jsou presne tak velke, aby obsahly
        // cisla jednotlivych dni + vertikalni padding.

        mWidth = columns * CalendarButton::WIDTH;
        mHeight = rows * CalendarButton::HEIGHT + TOP_OFFSET;
        Layout::setDimensions(mWidth, mHeight);
        listMonth();
        Layout::appendCreator(calendarCreator);

        calendarCreator.setParentPosition(position);
    }

    /* nextMonth připraví mFirstDayOfMonth na nový měsíc, zavolá předělání widgetů
     * a celý layout se updatuje
     */
    void nextMonth() {
        dateIterator.nextMonth();
        listMonth();
    }

    void previousMonth() {
        dateIterator.previousMonth();
        listMonth();
    }


    void listMonth() {
        std::tm timeNow = *gmtime(&dateIterator.getFirstDayOfMonth());

        // Smazání všech již nepotřebných čísel "datumů".
        int previousMonthNumberOfDays = dateIterator.getNumberOfDays();
        for (int k = 0; k < previousMonthNumberOfDays; k++) {
            string nameToErase = to_string(k + 1);
            calendarCreator.removeButtonByName(nameToErase);
        }

        int year = timeNow.tm_year + 1900;

        bool isLeap;
        if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) {
            isLeap = true;
        }else {
            isLeap = false;
        }

        pair<string,int> monthNameDays=Months::czech(isLeap).at(timeNow.tm_mon);

        string monthName=monthNameDays.first;
        int numberOfDays=monthNameDays.second;

        // Nastavení prvního dne měsíce.
        // Inicializováno jako záporné číslo.
        // Po iteraci s inkrementací od pondělí je pak den 1 prvním dnem k zobrazení.
        int day = -timeNow.tm_wday + 2;
        if (day == 2) { day = -5; }
        for (int i = 0; i < rows; i++) {
            // 7 cisel na radek. 5 znaku na cislo
            for (int j = 0; j < columns; j++) {
                // 5 znaku na cislo
                int buttonPositionX = j * CalendarButton::WIDTH;
                int buttonPositionY = i * CalendarButton::HEIGHT + TOP_OFFSET;
                if (day > 0 && day <= numberOfDays) {
                    cout << "init day: " << day << endl;
                    shared_ptr<CalendarButton> buttonToAdd = make_shared<CalendarButton>(day, Position(buttonPositionX,
                                                                                                       buttonPositionY));
                    buttonToAdd->setWidgetName(to_string(day));
                    buttonToAdd->setOnClickListener([this, day]() {
                        time_t iButtonTime = dateIterator.getFirstDayOfMonth() + DateIterator::daysToSeconds(day - 1);
                        mCalendarObserver(iButtonTime);
                    });
                    calendarCreator.addButton(buttonToAdd);
                }
                day++;
            }
        }

        monthName += " " + to_string(year);
        calendarCreator.findTextByName("monthNameText")->setText(monthName);
        Layout::update();
    }


    int getWidth() const override {
        return mWidth;
    }

    int getHeight() const override {
        return mHeight;
    }
};


#endif //SEMESTRALKA_CALENDARLAYOUT_H
