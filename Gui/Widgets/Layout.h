//
// Created by Frix on 28.06.2021.
//

/*
 * 300g dohromady
 * bucek
 * uzeny
 * balicek testovin
 */

#include <iostream>
#include <vector>
#include <set>
#include <cmath>
#include <memory>
#include "Widget.h"
#include "../../Pojo/LayoutChildrenContainer.h"
#include "../LayoutChildrenPriorityCompare.h"
#include "../../Navigation/NavigationComponent.h"
#include "../../Pojo/NavigationFocusablesNear.h"
#include "../LayoutCreator.h"
#include "../Updatable.h"

using namespace std;

#ifndef SEMESTRALKA_LAYOUT_H
#define SEMESTRALKA_LAYOUT_H

/// Funguje jako nosic widgetu. Pokud je master, vypisuje svuj obsah do konzole.
class Layout : public Widget, public Updatable {
    /// Pozadi layoutu plus propsani vsech widgetu. Obmeni se metodou refreshBitmap.
    vector<vector<char>> bitmap;

    // Mnozina potomku s jejich relativni pozici a prioritou
    //set<LayoutChildrenContainer, LayoutChildrenPriorityCompare> mChildWidgets{};

    vector<shared_ptr<Widget>> dummyWidget;
    vector<shared_ptr<Button>> dummyButton;

    // Seznam vsech widgetu bez specialni funkce.
    vector<shared_ptr<Widget>>& mBasicWidgets=dummyWidget;
    // Seznam vsech widgetu, ktere rozsiruji tridu Button
    vector<shared_ptr<Button>>& mButtons=dummyButton;
    // Seznam vsech zanorenych layoutu
    vector<shared_ptr<Layout>> mInnerLayouts;

    // Pokud je layout master může vypisovat do konzole.
    bool mMaster=false;

    LayoutCreator dummyCreator;
    // Predpis vsech widgetu, ktere ma obsahovat layout.
    LayoutCreator& mCreator=dummyCreator;

protected:
// Dimenze layoutu
    int mWidth{};
    int mHeight{};
public:
    explicit Layout(bool master, Position position)
            : Widget(position) {
        mMaster = master;
        setDimensions(1, 1);
    }

    // Uprava rozmeru layoutu.
    void setDimensions(int width, int height) {
        mWidth = width;
        mHeight = height;
        resized();
    }


    shared_ptr<Layout> &addInnerLayout(shared_ptr<Layout> &&layout) {
        mInnerLayouts.push_back(layout);
        mInnerLayouts[mInnerLayouts.size()-1]->setParentPosition(this->getPositionInLayout());
        layout->setOnBitmapChangeListener(this);
        return layout;
    }

    void appendCreator(LayoutCreator& creator){
        mCreator=creator;
        mButtons=mCreator.getButtons();
        mBasicWidgets=mCreator.getBasicWidgets();
        Position parentPosition = Widget::getParentPosition();
        mCreator.setParentPosition(parentPosition);
        mCreator.setOnBitmapChangeListener(this);
    }

    LayoutCreator &getCreator() const {
        return mCreator;
    }

    shared_ptr<Button> lastClickedButton;
    void initFocus() {
        lastClickedButton = nearestButton(Position(0, 0), NavigationComponent::RIGHT);
        lastClickedButton->onFocusStart();
        update();

    }

    void changeFocus(int navigationComponentDirection) {
        lastClickedButton->onFocusEnd();
        Position a=lastClickedButton->getPositionInLayout();
        Position b=lastClickedButton->getParentPosition();
        Position current = a+b;
        lastClickedButton = nearestButton(current, navigationComponentDirection);
        lastClickedButton->onFocusStart();

        update();
    }

    int getWidth() const override {
        return mWidth;
    }

    int getHeight() const override {
        return mHeight;
    }

    char getBitmapXY(int x, int y) const override {
        if (x >= mWidth || x < 0) {
            throw "Mimo oblast widgetu.";
        }
        if (y >= mHeight || y < 0) {
            throw "Mimo oblast widgetu.";
        }
        return (bitmap[y][x]);
    }

    // Vypsat obsah bitmapy do konzole.
    void update()override {
        refreshBitmap();
        if (mMaster) {
            for (int i = 0; i < 50; ++i) {
                cout << endl;
            }
            for (int i = 0; i < mHeight; ++i) {
                for (int j = 0; j < mWidth; j++) {
                    cout << bitmap[i][j];
                }
                cout << endl;
            }
        }
        Widget::forceBitmapUpdate();
    }

    // Nalezne nejblizsi button, ktery se da focusovat
    // Vrati nejblizsi CalendarButton, nejblizsi obyceny Button a binarni index, ktery je nejbliz.
    /*Button dummyBasic;
    Button&
    findNearestButtons(Position position,int navigationComponentDirection) {
        // Vhodne pro vychozi nastaveni focusu.
        return findNearestButtons(Button(position),navigationComponentDirection);
    }*/

    /// Slouzi k vypoctu vzdalenosti dvou widgetu.
    /// @param consoleToSquareRatio pomer fyzicke velikosti pismene v konzoli [vyska:sirka]
    /// @param pos1 pozice jednoho widgetu
    /// @param pos2 pozice druheho widgetu
    static float positionDistance(Position pos1, Position pos2, float consoleToSquareRatio = 2.5) {
        int xDiff = abs(pos1.getX() - pos2.getX());
        int yDiff = abs(pos1.getY() - pos2.getY()) * consoleToSquareRatio;

        float distance = sqrt(pow(xDiff, 2) + pow(yDiff, 2));
        if (floor(distance) == 25) {
            cout<<"Distance: "<<distance<<endl;
            cout<<"xDiff: "<<xDiff<<" ' yDiff: "<<yDiff<<endl;
        }
        return distance;
    }

    shared_ptr<Button>
    nearestButton(Position position, int navigationComponentDirection) {
        shared_ptr<Button> r;
        if (!mButtons.empty()) {
            r = mButtons[0];
        } else {
            throw "Prace s tlacitky na beztlacitkovem layoutu";
        }
        float minimalPositionDistance = 99999;
        for (const auto &mBasicButton : mButtons) {
            if (NavigationComponent::positionSquare(position,
                                                    mBasicButton->getPositionInLayout())
                == navigationComponentDirection) {
                float currentDistance = positionDistance(position, mBasicButton->getPositionInLayout(), 2.5);
                if (currentDistance <
                    minimalPositionDistance) {
                    minimalPositionDistance = currentDistance;
                    r = mBasicButton;
                }
            }
        }
        cout << "MINIMAL DISTANCE: " << minimalPositionDistance << endl;
        for (const auto &innerLayout : mInnerLayouts) {
            for (const auto &mBasicButton:innerLayout->getButtons()) {
                int absoluteX = mBasicButton->getPositionInLayout().getX() + innerLayout->getPositionInLayout().getX();
                int absoluteY = mBasicButton->getPositionInLayout().getY() + innerLayout->getPositionInLayout().getY();

                if (NavigationComponent::positionSquare(position,
                                                        Position(absoluteX,absoluteY))
                    == navigationComponentDirection) {
                    cout<<"Den: "<<mBasicButton->getBitmapXY(1,0)<<mBasicButton->getBitmapXY(2,0)<<endl;

                    Position foreignButtonPosition = Position(
                            absoluteX,
                            absoluteY);
                    float currentDistance = positionDistance(position, foreignButtonPosition, 2.5);
                    cout<<"Distance: "<<currentDistance<<endl;
                    if (currentDistance <
                        minimalPositionDistance) {
                        minimalPositionDistance = currentDistance;
                        r = mBasicButton;
                        cout<<"- Chosen"<<endl;
                    }
                }
            }
        }

        cout << "MINIMAL DISTANCE FOREIGN: " << minimalPositionDistance << endl;
        return r;
    }

    int clickFocusedButton() {
        lastClickedButton->click();
        return 1;
    }


private:
    // Smazat bitmapu a nahradit vychozimi hotnotami.
    // Nesmaze potomky. Ty se pohou funkci update() opet vratit na obrazovku.
    void clr() {
        char backgroundSymbol = '-';
        for (auto &i : bitmap) {
            for (char &j : i) {
                j = backgroundSymbol;
            }
        }
    }


    // Pokud se zmeni velikost layoutu znova nacist Widgety.
    void resized() {
        bitmap.resize(mHeight);
        for (int i = 0; i < mHeight; i++) {
            vector<char> widthVector;
            widthVector.resize(mWidth);
            bitmap[i] = widthVector;
        }
        refreshBitmap();
        update();
    }

    // Promitnout vsechny Widget potomky do bitmapy, podle prioritniho poradi v mnozine.
    void refreshBitmap() {
        clr();

        for (auto &cont : mButtons) {
            writeWidget(reinterpret_cast<shared_ptr<struct Widget> &>(cont));
        }
        for (auto &cont : mBasicWidgets) {
            writeWidget(reinterpret_cast<shared_ptr<Widget> &>(cont));
        }

        for (auto &cont : mInnerLayouts) {
            writeWidget(reinterpret_cast<shared_ptr<struct Widget> &>(cont));
        }
    }

    // Zapsat widget do bitmapy.
    // Pokud widget presahne bitmapu layoutu, widget se presune mimo layout.
    void writeWidget(shared_ptr<Widget> &widget) {
        Position pos = widget->getPositionInLayout();
        int verticalIterate = min(widget->getHeight() + pos.getY(), mHeight);
        int horizontalIterate = min(widget->getWidth() + pos.getX(), mWidth);
        for (int i = pos.getY(); i < verticalIterate; i++) {
            for (int j = pos.getX(); j < horizontalIterate; j++) {
                int x = j - pos.getX();
                int y = i - pos.getY();
                char r = widget->getBitmapXY(x, y);
                bitmap[i][j] = r;
            }
        }
    }

    const vector<shared_ptr<Button>> &getButtons() const {
        return mButtons;
    }

};


#endif //SEMESTRALKA_LAYOUT_H
