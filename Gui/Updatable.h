//
// Created by Frix on 30.07.2021.
//

#ifndef SEMESTRALKA_UPDATABLE_H
#define SEMESTRALKA_UPDATABLE_H


class Updatable {
public:
    virtual void update(){}
};


#endif //SEMESTRALKA_UPDATABLE_H
