//
// Created by Frix on 01.07.2021.
//

#ifndef SEMESTRALKA_FOCUSCONTROLLER_H
#define SEMESTRALKA_FOCUSCONTROLLER_H


#include <stack>
#include "Widgets/Layout.h"

class FocusController {
    std::stack<Layout> stack;

    bool focusOuterLayout() {
        if (!stack.empty()) {
            stack.pop();
            return true;
        } else {
            return false;
        }
    }
    void focusInnerLayout(const Layout& l){
       // stack.push(l);
    }
};


#endif //SEMESTRALKA_FOCUSCONTROLLER_H
