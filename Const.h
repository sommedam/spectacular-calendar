//
// Created by Frix on 28.06.2021.
//

#ifndef SEMESTRALKA_CONST_H
#define SEMESTRALKA_CONST_H

#include <string>

using namespace std;

/// Slouzi pro uchovani konstant.
class Const {
public:
   const string FRAGMENT_DETAIL_EXCHANGE_CHOOSEN_DAY = "timeday";
   const string FRAGMENT_NAME_CALENDAR = "CalendarFragment";
   const string FRAGMENT_NAME_DETAIL = "DetailFragment";
};


#endif //SEMESTRALKA_CONST_H
