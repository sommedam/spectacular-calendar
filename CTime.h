//
// Created by Frix on 28.06.2021.
//

#ifndef SEMESTRALKA_CTIME_H
#define SEMESTRALKA_CTIME_H


#include <iostream>

using namespace std;

class CTime {
    int mYears;
    int mMonths;
    int mDays;
    int mHours;
    int mMinutes;
    int mSeconds;
public:
    CTime() : CTime(0, 0, 0,0,0,0) {}

    CTime(int mYears, int mMonths, int mDays, int mHours, int mMinutes, int mSeconds) : mYears(mYears), mMonths(mMonths),
                                                                                        mDays(mDays), mHours(mHours),
                                                                                        mMinutes(mMinutes),
                                                                                        mSeconds(mSeconds) {
        if(validateTime(mYears, mMonths, mDays, mHours, mMinutes, mSeconds)==0){
            throw "CTIME - Spatny cas";
        }
    }

    CTime(int hours, int minutes) : mHours(hours), mMinutes(minutes) {
        checkMod(hours, 24);
        checkMod(minutes, 60);

        mHours = hours;
        mMinutes = minutes;
        mSeconds = 0;
    }

    static int validateTime(int years, int months, int days, int hours, int minutes,int seconds) {
        int monthLonginess[12] = {
                31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
        };

        if (days < 1) {
            return 0;
        };
        if (months > 12 || months < 1) {
            return 0;
        }

        if (hours > 23 || hours < 0) {
            return 0;
        }
        if (minutes > 59 || minutes < 0) {
            return 0;
        }

        if(seconds > 60 || seconds<0){
            return 0;
        }

        if (monthLonginess[months - 1] < days) {
            return 0;
        } else {
            return 1;
        }
    }

    bool inRange(CTime aExtreme, CTime bExtreme) {
        if (aExtreme > bExtreme) {
            swap(aExtreme, bExtreme);
        }
        if ((*this) <= bExtreme && (*this) >= aExtreme) {
            return true;
        } else {
            return false;
        }
    }

    /*CTime operator++(int) {
        CTime tmpTime;
        tmpTime = *this;
        tmpTime = tmpTime + 1;
        *this=tmpTime;
        return tmpTime;
    }*/

    CTime& operator = (const CTime assignTime){
        this->mSeconds=assignTime.mSeconds;
        this->mMinutes=assignTime.mMinutes;
        this->mHours=assignTime.mHours;
        this->mDays=assignTime.mDays;
        this->mMonths = assignTime.mMonths;
        this->mYears=assignTime.mYears;
        return *this;
    }

    bool operator==(const CTime secondTime) const {
        if (secondTime.mYears == mYears &&
            secondTime.mMonths == mMonths &&
            secondTime.mDays == mDays &&
            secondTime.mHours == mHours &&
            secondTime.mMinutes == mMinutes &&
            secondTime.mSeconds == mSeconds) {
            return true;
        } else {
            return false;
        }
    }

    bool operator <=(const CTime secondTime) const{
        if(secondTime==*this ||
        *this<secondTime){
            return true;
        }
        return false;
    }
    bool operator >=(const CTime secondTime) const{
        if(secondTime==*this ||
           *this>secondTime){
            return true;
        }
        return false;
    }

   bool operator>(const CTime secondTime) const {
        if(*this==secondTime){
            return false;
        } else{
            return !(*this<secondTime);
        }
    }
    bool operator<(const CTime secondTime) const {
        int yearDiff = secondTime.mYears - mYears;
        int monthDiff = secondTime.mMonths - mMonths;
        int dayDiff = secondTime.mDays - mDays;
        int hourDiff = secondTime.mHours - mHours;
        int minuteDiff = secondTime.mMinutes - mMinutes;
        int secondsDiff = secondTime.mSeconds - mSeconds;

        if (yearDiff == 0) {
            if (monthDiff == 0) {
                if (dayDiff == 0) {
                    if (hourDiff == 0) {
                        if (minuteDiff == 0) {
                            if (secondsDiff == 0) {
                            } else if (secondsDiff < 0) {
                                return false;
                            } else {
                                return true;
                            }
                        } else if (minuteDiff < 0) {
                            return false;
                        } else {
                            return true;
                        }
                    } else if (hourDiff < 0) {
                        return false;
                    }
                } else if (dayDiff < 0) {
                    return false;
                }
            } else if (monthDiff < 0) {
                return false;
            }
        } else if (yearDiff < 0) {
            return false;
        } else {
            return true;
        }
        return false;
    }

    /*CTime operator+(const int &seconds) const {
        CTime ret = *this;

        int daysToAdd = (seconds/86400);
        int hoursToAdd = (seconds / 3600);
        int minutesToAdd = (seconds % 3600) / 60;
        int secondsToAdd = (seconds % 3600 % 60);

        cout << "hours " << hoursToAdd << "\n";
        cout << "minutes " << minutesToAdd << "\n";
        cout << "seconds " << secondsToAdd << "\n";

        ret.mSeconds = ret.mSeconds + secondsToAdd;
        ret.mMinutes = ret.mSeconds / 60 + ret.mMinutes + minutesToAdd;
        ret.mSeconds = ret.mSeconds % 60;
        ret.mHours = ret.mMinutes / 60 + ret.mHours + hoursToAdd;
        cout << "tmp h: " << ret.mHours << "\n";
        ret.mMinutes = ret.mMinutes % 60;

        ret.mHours = ret.mHours % 24;

        return ret;
    }

    CTime operator+=(const int &seconds) {
        CTime tmpTime = *this;
        tmpTime = tmpTime + seconds;

        this->mSeconds = tmpTime.mSeconds;
        this->mMinutes = tmpTime.mMinutes;
        this->mHours = tmpTime.mHours;

        return *this;
    }*/

    CTime operator-(const int &seconds) const {
        CTime ret = *this;
        int modSeconds = seconds % 86400;

        int hoursToDec = (modSeconds / 3600);
        int minutesToDec = (modSeconds % 3600) / 60;
        int secondsToDec = (modSeconds % 3600 % 60);

        cout << "hours " << hoursToDec << "\n";
        cout << "minutes " << minutesToDec << "\n";
        cout << "seconds " << secondsToDec << "\n";

        ret.mSeconds -= secondsToDec;
        if (ret.mSeconds < 0) {
            minutesToDec += 1;
            ret.mSeconds += 60;
        }
        ret.mMinutes -= minutesToDec;
        if (ret.mMinutes < 0) {
            hoursToDec += 1;
            ret.mMinutes += 60;
        }

        //cout<<ret.mHours<<"\n";
        //cout<<hoursToDec<<"\n";
        ret.mHours -= hoursToDec;

        cout << ret.mHours << "\n";
        if (ret.mHours < 0) { ret.mHours += 24; }
        return ret;
    }

    CTime operator-=(const int &seconds) {
        CTime tmpTime = *this;
        tmpTime = tmpTime - seconds;

        this->mSeconds = tmpTime.mSeconds;
        this->mMinutes = tmpTime.mMinutes;
        this->mHours = tmpTime.mHours;

        return *this;
    }

    const CTime operator--(int) {
        CTime tmpTime;
        tmpTime = *this;
        tmpTime = tmpTime - 1;
        this->mSeconds = tmpTime.mSeconds;
        this->mMinutes = tmpTime.mMinutes;
        this->mHours = tmpTime.mHours;
        return tmpTime;
    }

    static void checkMod(int time, int mod) {
        if (time < 0 || time >= mod) {
            throw std::invalid_argument("Číslo mimo rozsah");
        }
    }

    bool checkDaysValid(int m, int d){
        int monthLonginess[12] = {
                31,
                28,
                31,
                30,
                31,
                30,
                31,
                31,
                30,
                31,
                30,
                31
        };
        if(monthLonginess[m-1]<d){return false;}else{
            return true;
        }
    }

    friend std::ostream &operator<<(std::ostream &os, const CTime &time) {
        if (time.mHours < 10) {
            os << "0";
            if (time.mHours == 0) {
                os << "0";
            } else {
                os << time.mHours;
            }
        } else { os << time.mHours; }
        os << ":";
        if (time.mMinutes < 10) {
            os << "0";
            if (time.mMinutes == 0) {
                os << "0";
            } else {
                os << time.mMinutes;
            }
        } else { os << time.mMinutes; }
        os << ":";
        if (time.mSeconds < 10) {
            os << "0";
            if (time.mSeconds == 0) {
                os << "0";
            } else {
                os << time.mSeconds;
            }
        } else { os << time.mSeconds; }
        return os;
    }
};


#endif //SEMESTRALKA_CTIME_H
