//
// Created by Frix on 02.08.2021.
//

#ifndef SEMESTRALKA_FRAGMENT_H
#define SEMESTRALKA_FRAGMENT_H


#include "FragmentListener.h"
#include "ExchangeBundle.h"

class Fragment {
public:
    /// Zavola se pri aktivaci fragmentu. Pri behu aplikace se muze zavolat vicekrat.
    virtual void onCreate(FragmentListener * fragmentHandler,ExchangeBundle & bundle){}
};


#endif //SEMESTRALKA_FRAGMENT_H
