//
// Created by Frix on 04.08.2021.
//

#ifndef SEMESTRALKA_EXCHANGEBUNDLE_H
#define SEMESTRALKA_EXCHANGEBUNDLE_H

#include <string>
#include <map>

using namespace std;
class ExchangeBundle {
    map<string, string> mBundle;

    explicit ExchangeBundle(map<string,string> map){
        mBundle=move(map);
    }

public:
    const map<string, string> &getBundle() const {
        return mBundle;
    }

    string dummyString;
    // Vrací dotázaný element. Pokud neexistuje vrací prázdný string.
    // nevyvola exception, protoze pri behu je potreba dotazovat i nepridane elementy
    string &getBundleElement(string &elementName){
        auto r = mBundle.find(elementName);
        if(!mBundle.empty()&& r!=mBundle.end()){
            return r->second;
        }else{
            return dummyString;
        }
    }

    void setBundleElement(string  elementName, string  content){
        mBundle[move(elementName)]=move(content);
    }

    ExchangeBundle()= default;
};


#endif //SEMESTRALKA_EXCHANGEBUNDLE_H
