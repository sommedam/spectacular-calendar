//
// Created by Frix on 01.07.2021.
//

#ifndef SEMESTRALKA_LAYOUTCHILDRENHASH_H
#define SEMESTRALKA_LAYOUTCHILDRENHASH_H


#include "Pojo/LayoutChildrenContainer.h"

class LayoutChildrenHash {
public:
    long operator()(const LayoutChildrenContainer &a) const {
        return a.getWidget().getUniqueId();
    }
};


#endif //SEMESTRALKA_LAYOUTCHILDRENHASH_H
