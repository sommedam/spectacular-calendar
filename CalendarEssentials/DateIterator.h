//
// Created by sommedam on 2/10/22.
//

#ifndef SPECTACULAR_CALENDAR_DATEITERATOR_H
#define SPECTACULAR_CALENDAR_DATEITERATOR_H


#include <ctime>
#include <iostream>
using namespace std;
class DateIterator {
    time_t mFirstDayOfMonth{};

    int numberOfDays = 0;
public:
     explicit DateIterator(){
        defaultTime();
     }

    void defaultTime() {
        mFirstDayOfMonth = time(nullptr);
        mFirstDayOfMonth += 60 * 60 * 2;
        std::tm t = *gmtime(&mFirstDayOfMonth);

        cout<< "t: " << std::asctime(&t) << endl;
        // GMT+2
        // Odecist pocet dni, aby se nastavil prvni den mesice
        mFirstDayOfMonth -= ((t.tm_mday - 1) * 24 * 60 * 60);
    }

    void nextMonth(){
         // TODO: 31 muze pretect nasledujici rok
        int secondsToNextMonth = daysToSeconds(31);
        mFirstDayOfMonth += secondsToNextMonth;
        std::tm t = {};
        t = *gmtime(&mFirstDayOfMonth);
        mFirstDayOfMonth -= daysToSeconds(t.tm_mday - 1);

    }

    void previousMonth(){
        int secondsToPreviousMonth = daysToSeconds(1);
        mFirstDayOfMonth -= secondsToPreviousMonth;
        std::tm t = {};
        t = *gmtime(&mFirstDayOfMonth);
        mFirstDayOfMonth -= daysToSeconds(t.tm_mday - 1);
    }

    static int daysToSeconds(int d) {
        return d * 24 * 60 * 60;
    }

    time_t& getFirstDayOfMonth()  {
         time_t & r=mFirstDayOfMonth;
        return r;
    }


    int getNumberOfDays() const {
        return numberOfDays;
    }
};




#endif //SPECTACULAR_CALENDAR_DATEITERATOR_H
