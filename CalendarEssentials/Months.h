//
// Created by sommedam on 2/10/22.
//

#ifndef SPECTACULAR_CALENDAR_MONTHS_H
#define SPECTACULAR_CALENDAR_MONTHS_H

#include <string>
#include <vector>

using namespace std;
class Months {
public:
    // pole mesicu a poctu dni, kolik trvaji
    static vector<pair<string,int>> czech(bool isLeap){
        vector<pair<string,int>> months;
        months.emplace_back(pair<string,int>("Leden",31));

        if(isLeap) {
            months.emplace_back(pair<string, int>("Unor", 29));
        }else{
            months.emplace_back(pair<string,int>("Unor",28));
        }

        months.emplace_back(pair<string,int>("Brezen",31));

        months.emplace_back(pair<string,int>("Duben",30));

        months.emplace_back(pair<string,int>("Kveten",31));

        months.emplace_back(pair<string,int>("Cerven",30));

        months.emplace_back(pair<string,int>("Cervenec",31));

        months.emplace_back(pair<string,int>("Srpen",31));

        months.emplace_back(pair<string,int>("Zari",30));

        months.emplace_back(pair<string,int>("Rijen",31));

        months.emplace_back(pair<string,int>("Listopad",30));

        months.emplace_back(pair<string,int>("Prosinec",31));
        return months;
    }

};


#endif //SPECTACULAR_CALENDAR_MONTHS_H
