//
// Created by Frix on 04.08.2021.
//

#ifndef SEMESTRALKA_DETAILCREATOR_H
#define SEMESTRALKA_DETAILCREATOR_H


#include "../Gui/LayoutCreator.h"
#include "../Const.h"

class DetailCreator: public LayoutCreator{
public:
    DetailCreator(){
        Const constaints;

        shared_ptr<Text> plannerInput=make_shared<Text>("Pridat novy plan.",Position(30,2));
        shared_ptr<Text> monthNameText=make_shared<Text>("[TIME]",Position(28,0));

        monthNameText->setWidgetName(constaints.FRAGMENT_DETAIL_EXCHANGE_CHOOSEN_DAY);
        LayoutCreator::addText(plannerInput);
        LayoutCreator::addText(monthNameText);
    }
};


#endif //SEMESTRALKA_DETAILCREATOR_H
