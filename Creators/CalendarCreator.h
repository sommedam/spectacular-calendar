//
// Created by Frix on 25.07.2021.
//

#ifndef SEMESTRALKA_CALENDARCREATOR_H
#define SEMESTRALKA_CALENDARCREATOR_H


#include "../Gui/LayoutCreator.h"

class CalendarCreator: public LayoutCreator {

public:
    CalendarCreator(){
        LayoutCreator::mBasicWidgets.push_back(make_shared<Text>("Po",Position(1,1)));
        LayoutCreator::mBasicWidgets.push_back(make_shared<Text>("Ut",Position(4,1)));
        LayoutCreator::mBasicWidgets.push_back(make_shared<Text>("St",Position(9, 1)));
        LayoutCreator::mBasicWidgets.push_back(make_shared<Text>("Ct",Position(13,1)));
        LayoutCreator::mBasicWidgets.push_back(make_shared<Text>("Pa",Position(17,1)));
        LayoutCreator::mBasicWidgets.push_back(make_shared<Text>("So",Position(21,1)));
        LayoutCreator::mBasicWidgets.push_back(make_shared<Text>("Ne",Position(25,1)));
        shared_ptr<Text> monthNameText = make_shared<Text>("",Position(9,0));
        monthNameText->setWidgetName("monthNameText");
        LayoutCreator::addText(monthNameText);
    }

};


#endif //SEMESTRALKA_CALENDARCREATOR_H
