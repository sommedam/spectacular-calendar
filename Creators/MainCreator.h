//
// Created by Frix on 23.07.2021.
//

#ifndef SEMESTRALKA_MAINCREATOR_H
#define SEMESTRALKA_MAINCREATOR_H


#include "../Gui/LayoutCreator.h"

class MainCreator: public LayoutCreator {

public:
    MainCreator(){
        shared_ptr<TextButton> nextButton=make_shared<TextButton>("Next",Position(59,2));
        nextButton->setWidgetName("nextButton");
        shared_ptr<TextButton> previousButton=make_shared<TextButton>("Previous",Position(12,2));

        previousButton->setWidgetName("previousButton");
        LayoutCreator::mButtons.push_back(nextButton);
        LayoutCreator::nameIndexedButtons.emplace(nextButton);
        LayoutCreator::mButtons.push_back(previousButton);
        LayoutCreator::nameIndexedButtons.emplace(previousButton);
        LayoutCreator::mBasicWidgets.push_back(make_shared<Text>("Calendrier gregorien (de Gregoire XIII)", Position(21, 0)));
        LayoutCreator::mBasicWidgets.push_back(make_shared<Text>("[p] previous | [n] next | [*] date details", Position(0,13)));
    }
};


#endif //SEMESTRALKA_MAINCREATOR_H
