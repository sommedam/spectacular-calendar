//
// Created by Frix on 12.08.2021.
//

#ifndef SEMESTRALKA_PLANNEREVENTCREATOR_H
#define SEMESTRALKA_PLANNEREVENTCREATOR_H


#include "../Gui/LayoutCreator.h"
#include "../Pojo/Event.h"

class PlannerEventCreator: public LayoutCreator {
public:
    shared_ptr<Text> beginText;
    shared_ptr<Text> endText;
    shared_ptr<Text> description;

    explicit PlannerEventCreator(const Event& event,int order){
        beginText=make_shared<Text>("",Position(0,0));
        string leftDecoration = "#";
        if(order%2){
            leftDecoration = " ";
        }
        leftDecoration+=' ';
        time_t eventBegin = event.getBegin();
        string beginString = leftDecoration+ dateText(eventBegin);
        beginText->setText(beginString);

        endText=make_shared<Text>("",Position(0,1));
        time_t eventEnd = event.getEnd();
        string endString = leftDecoration + dateText(eventEnd);
        endText->setText(endString);

        description = make_shared<Text>(event.getDescription(),Position(10,0));

        LayoutCreator::addText(endText);
        LayoutCreator::addText(beginText);
        LayoutCreator::addText(description);
    }

    // hh:mm
private:
    string dateText(time_t & time){
        std::tm t = {};
        t = *gmtime(&time);
        string hours = to_string(t.tm_hour);
        string minutes = to_string(t.tm_min);
        if(hours.size()<2){
            hours='0' + hours;
        }
        if(minutes.size()<2){
            minutes='0' + minutes;
        }
        return hours+":"+minutes;
    }
};


#endif //SEMESTRALKA_PLANNEREVENTCREATOR_H
