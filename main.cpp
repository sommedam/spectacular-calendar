#include <iostream>
#include "Fragment/CalendarFragment.h"
#include "Fragment/FragmentHandler.h"
#include "Fragment/DetailFragment.h"
#include <thread>

using namespace std;


int main() {
    // Inicializace vsech fragmentu
    map<std::string, shared_ptr<Fragment>> fragments;
    Const constants;
    fragments[constants.FRAGMENT_NAME_CALENDAR] = make_shared<CalendarFragment>();
    fragments[constants.FRAGMENT_NAME_DETAIL] = make_shared<DetailFragment>();
    // Predani fragmentu do handleru
    FragmentHandler fh = FragmentHandler("CalendarFragment",fragments);

    return 0;
}