//
// Created by Frix on 23.07.2021.
//

#ifndef SEMESTRALKA_LAYOUTCREATOR_H
#define SEMESTRALKA_LAYOUTCREATOR_H


#include <memory>
#include <set>
#include "Widgets/Widget.h"
#include "Widgets/Button.h"
#include "LayoutChildrenHash.h"
#include "Widgets/Text.h"
#include "Widgets/TextButton.h"
#include "Widgets/CalendarButton.h"

using namespace std;

struct widgetNameCmp {
public:
    bool operator() (const shared_ptr<Widget>& lhs, const shared_ptr<Widget>& rhs) const {
        return (lhs->getWidgetName()) < rhs->getWidgetName();
    }
    bool operator() (const shared_ptr<Widget>& lhs, const string & rhs) const {
        return (lhs->getWidgetName()) < rhs;
    }
    bool operator() (const string& lhs, const shared_ptr<Widget>& rhs) const {
        return (lhs < rhs->getWidgetName());
    }
};

struct buttonNameCmp {
public:
    bool operator() (const shared_ptr<Button>& lhs, const shared_ptr<Button>& rhs) const {
        return (lhs->getWidgetName()) < rhs->getWidgetName();
    }
    bool operator() (const shared_ptr<TextButton>& lhs, const shared_ptr<TextButton>& rhs) const {
        return (lhs->getWidgetName()) < rhs->getWidgetName();
    }
    bool operator() (const shared_ptr<TextButton>& lhs, const string & rhs) const {
        return (lhs->getWidgetName()) < rhs;
    }
    bool operator() (const string& lhs, const shared_ptr<TextButton>& rhs) const {
        return (lhs < rhs->getWidgetName());
    }
};
struct textNameCmp {
public:
    bool operator() (const shared_ptr<Text>& lhs, const shared_ptr<Text>& rhs) const {
        return (lhs->getWidgetName()) < rhs->getWidgetName();
    }
};

class LayoutCreator {
public:
    vector<shared_ptr<Widget>> mBasicWidgets;
    vector<shared_ptr<Button>> mButtons;
    set<shared_ptr<Widget>,widgetNameCmp> nameIndexedWidgets;
    set<shared_ptr<Button>,buttonNameCmp> nameIndexedButtons;
    set<shared_ptr<Text>,textNameCmp> nameIndexedTexts;

    void addButton(shared_ptr<TextButton>& button){
        button->setParentPosition(mParentPosition);
        mButtons.push_back(button);
        if((!button->getWidgetName().empty())){
            nameIndexedButtons.emplace(button);
        }
    }
    void addButton(shared_ptr<CalendarButton>& button){
        button->setParentPosition(mParentPosition);
        mButtons.push_back(button);
        if((!button->getWidgetName().empty())){
            nameIndexedButtons.emplace(button);
        }
    }

    void addText(shared_ptr<Text>& text){
        text->setParentPosition(mParentPosition);
        mBasicWidgets.push_back(text);
        if((!text->getWidgetName().empty())){
            nameIndexedTexts.emplace(text);
        }
    }

    void removeButtonByName(string& name){
        std::set<shared_ptr<Button>,buttonNameCmp>::iterator it;
        long erasedId;

        shared_ptr<TextButton> txt = make_shared<TextButton>("",Position(0,0));
        txt->setWidgetName(name);
        it = nameIndexedButtons.find(txt);
        if(it==nameIndexedButtons.end()){
            throw "Zadaný widget neexistuje";
        }else{
            erasedId=it.operator*()->getUniqueId();
            nameIndexedButtons.erase(it);
            removeFromVector(erasedId);
        }
    }

    void removeFromVector(long searchId){
        for(int i =0; i<mButtons.size(); i++){
            if(mButtons[i]->getUniqueId()==searchId){
                mButtons.erase(mButtons.begin()+i);
            }
        }
    }

    shared_ptr<Text> findTextByName(const std::string& name){
        shared_ptr<Text> txt = make_shared<Text>("",Position(0,0));
        txt->setWidgetName(name);
        auto found = nameIndexedTexts.find(txt);
        if(found==nameIndexedTexts.end()){
            throw "Zadaný widget neexistuje";
        }else{
            return found.operator*();
        }
    }


    shared_ptr<Widget> findWidgetByName(const std::string& name){
        shared_ptr<Text> txt = make_shared<Text>("",Position(0,0));
        txt->setWidgetName(name);
        auto found = nameIndexedWidgets.find(txt);
        if(found==nameIndexedWidgets.end()){
            throw "Zadaný widget neexistuje";
        }else{
            return found.operator*();
        }
    }
    shared_ptr<Button> findButtonByName(const std::string name){
        shared_ptr<TextButton> txt = make_shared<TextButton>("",Position(0,0));
        txt->setWidgetName(name);
        auto found = nameIndexedButtons.find(txt);
        if(found==nameIndexedButtons.end()){
            throw "Zadaný widget neexistuje";
        }else{
            return found.operator*();
        }
    }

    const vector<shared_ptr<struct Widget>> & getBasicWidgets()  {
        return mBasicWidgets;
    }

    const vector<shared_ptr<struct Button>> & getButtons()  {
        return mButtons;
    }

    // Nastavení relativní pozice nadřazeného Layoutu.
    Position mParentPosition=Position(0,0);
    void setParentPosition(Position& parentPosition){
        mParentPosition=parentPosition;
        for (auto& w: mButtons) {
            w->setParentPosition(mParentPosition);
        }
        for (auto& w: mBasicWidgets) {
            w->setParentPosition(mParentPosition);
        }
    }

public:
    void setOnBitmapChangeListener(Updatable * observer){
        for(auto &w:mBasicWidgets){
            w->setOnBitmapChangeListener(observer);
        }
        for(auto &w:mButtons){
            w->setOnBitmapChangeListener(observer);
        }
    }
};


#endif //SEMESTRALKA_LAYOUTCREATOR_H
