//
// Created by Frix on 01.07.2021.
//

#ifndef SEMESTRALKA_LAYOUTCHILDRENPRIORITYCOMPARE_H
#define SEMESTRALKA_LAYOUTCHILDRENPRIORITYCOMPARE_H


#include "Pojo/LayoutChildrenContainer.h"

class LayoutChildrenPriorityCompare {
public:
    bool operator()(const LayoutChildrenContainer &a, const LayoutChildrenContainer &b) const {
        if (a.getPriority() < b.getPriority()) {
            return true;
        }
        return false;
    }
};


#endif //SEMESTRALKA_LAYOUTCHILDRENPRIORITYCOMPARE_H
