//
// Created by Frix on 02.08.2021.
//

#ifndef SEMESTRALKA_FRAGMENTLISTENER_H
#define SEMESTRALKA_FRAGMENTLISTENER_H


#include <map>
#include <functional>
#include <memory>
#include "KeyMapComparator.h"
#include "ExchangeBundle.h"

class FragmentListener {


public:
    virtual void onKeyMapChange(std::map<char, std::function<void()>>&keyMap){}
    virtual void onFragmentChange(std::string& fragmentName, ExchangeBundle & exchangeBundle){}
};


#endif //SEMESTRALKA_FRAGMENTLISTENER_H
