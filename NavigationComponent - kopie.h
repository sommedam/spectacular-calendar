//
// Created by Frix on 30.06.2021.
//

#ifndef SEMESTRALKA_NAVIGATIONCOMPONENT_H
#define SEMESTRALKA_NAVIGATIONCOMPONENT_H


#include <cstdlib>
#include <iostream>
#include "Widgets/Focusable.h"
#include "Pojo/Position.h"

class NavigationComponent {

    // Pismena v konzoli jsou priblizne 2.5x vyssi nez sirsi.
    constexpr static const float consoleToSquareRatio = 2.5;

public:

    static const int LEFT = 0;
    static const int RIGHT = 1;
    static const int TOP = 2;
    static const int BOTTOM = 3;
    static const int SAME = 4;

    // Zjisti na ktere strane se od base Widgetu nachazi druhy Widget.
    // Metoda nepocita s sirkou objektu. Doporucuje se pouzit pozici stredu objektu.
    static int positionSquare(Position positionBase,
                              Position positionCandidate) {
        if(positionBase==positionCandidate){return SAME;}
        int baseX = positionBase.getX();
        int baseY = positionBase.getY();
        int candidateX = positionCandidate.getX();
        int candidateY = positionCandidate.getY();

       // if (candidateX == baseX) { if (candidateY > baseY) { return TOP; } else { return BOTTOM; }}
       // if (candidateY == baseY) { if (candidateX > baseX) { return RIGHT; } else { return LEFT; }}

        int diffX = baseX - candidateX;
        int diffY = baseY - candidateY;


        int relative = abs((int) (candidateX / consoleToSquareRatio));
        if (abs(candidateY) > relative) {
            if (diffY > 0) {
               // std::cout << "Top";
                return TOP;
            } else {
                //std::cout << "Bottom";
                return BOTTOM;
            }
        } else {
            if (diffX < 0) {
               // std::cout << "Right";
                return RIGHT;
            } else {
               // std::cout << "Left";
                return LEFT;
            }
        }
    }

};


#endif //SEMESTRALKA_NAVIGATIONCOMPONENT_H
